# test-toolbox
Desarrollador: Carlos Javier Mendoza Hernandez.

Correo: cmendoza5806@yahoo.com


Prueba de backend developer de Toolbox.
Tecnología:  NodeJS.

## Descripción:

A - Programar un API en Nodejs, a la cual se le pase un texto y responda con el mismo texto.
B - Programar un script que consuma el API creada en el punto A, el cual se ejecute de la siguiente forma y muestre resultados: $ node client.js "example text"

Se pide:
Un API en Nodejs 8 usando Express, junto con sus test unitarios usando Chai & Supertest
Un cliente funcional del API usando Nodejs 8 y request-promise.

Enviar el código fuente junto con las instrucciones de cómo correr las apps y los test.

Opcional:
Usar Docker o Docker Compose para correr las apps
Entregar el código en un repo git *

## Para instalar

1. Clonar el proyecto en https://gitlab.com/kachejunior/test-toolbox.git

2. Ejecutar en consola `npm install` que instalará todas las dependencias que necesitará el proyecto, se encuentran en el archivo package.json

3. Ejecutar `npm start` o `node app.js` y el servicio se levantara en http://localhost:3000

4. Ejecutar el siguiente comando en una segunda consola. `node client.js [argumento]` donde _argumento_ es el texto que deseas mandar al servicio app.js

5. Para ejecutar test usar el comando `npm test` o `./node_modules/mocha/bin/mocha test.js`
 

