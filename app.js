
var express = require('express');
var app = express();
var bodyParser = require('body-parser');
app.use(bodyParser.json());
app.use(bodyParser.urlencoded({ extended: true }));


/**
 * Indica que el servicio se encuentra levantado sea por browser o un get a la url base
 */
app.get('/', function (req, res) {
    res.send('El servicio test-toolbox se encuentra levantado!');
});


/**
 *Servicio http post para la prueba, se envia un texto y devolvera el mismo texto.
 */
app.post('/', function (req, res) {
    console.log(req.body);
    try {
        if(req.body.hasOwnProperty('texto')) { //Determina si el atributo texto a sido recibido
            res.status(200).send(req.body.texto);
        }
        else {
            res.status(400).send('El atributo texto no ha sido enviado');
        }
        
    }
    catch (error) {
        res.status(500).send('Un error a ocurrido');
    }

});

app.listen(3000, function () {
    console.log('Servidor activo en puerto 3000!');
});

module.exports = app
