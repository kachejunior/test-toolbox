const request = require('supertest');
const app = require('./app');


/**
 * Funcion para devolver un string aleatorio
 * @length - Tamaño del string
 */
function randomString(length) {
    var text = '';
    var possible = '123456abcdefghijkmnlopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ ';
    for (var i = 0; i < length; i++) {
        text += possible[Math.floor(Math.random() * possible.length)];
    }
    return text;
}

describe('POST /', function () {
    it('Responder con 200 en caso de exito', function (done) {
        request(app)
            .post('/')
            .send({"texto": randomString(Math.floor(Math.random() * 100))}) //permite que se envie un string con un rango variable entre 0 a 100 caracteres
            .set('Content-Type', 'application/json')
            .set('Accept', 'application/json')
            .expect(200)
            .end((err) => {
                if (err) return done(err);
                done();
            });
    });
});