var rp = require('request-promise');

/**
 * Validacion en caso de no haber facilitado un argumento por comando
 */
if (process.argv.length < 3) {
    console.log('La operacion no puede procederse sin algun argumento insertado');
    return false;
}

var texto = process.argv[2]; //Captura del argumento

/**
 * Configuracion de la peticion http
 */
var options = {
    method: 'POST',
    uri: 'http://localhost:3000',
    body: {
        texto: texto
    },
    json: true
};

/**
 * Peticion por promise
 */
rp(options)
    .then(function (res) {
        console.log(res);
    })
    .catch(function (err) {
        console.log('Error ' + err.message);
    });